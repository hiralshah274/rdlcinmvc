﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace RDLCInMVC.Reports
{
    public partial class CompanyRDLC : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<Company> companies = new List<Company>();
                companies.Add(new Company { CompanyID = 1, CompanyName = "Apple", Description = "apple" });
                companies.Add(new Company { CompanyID = 2, CompanyName = "Samsung" });
                companies.Add(new Company { CompanyID = 3, CompanyName = "Redmi" });

                rvCompanyList.LocalReport.ReportPath = Server.MapPath("~/Reports/TempReport.rdlc");
                rvCompanyList.LocalReport.DataSources.Clear();
                ReportDataSource rdc = new ReportDataSource("RDLCDatabaseDataSet", companies);
                rvCompanyList.LocalReport.DataSources.Add(rdc);
                rvCompanyList.LocalReport.Refresh();
                rvCompanyList.DataBind();
            }
        }
    }
}